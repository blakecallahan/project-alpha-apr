from django.urls import path
from projects.views import (
    show_projects,
    show_project_details,
    create_project,
)


urlpatterns = [
    path("", show_projects, name="list_projects"),
    path("<int:id>/", show_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
